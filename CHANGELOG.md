# Changelog
All notable changes to this module will be documented in this file.

## [0.1] - 2020-10-12
### Added
- Initial release.
